import axios from 'axios';

axios.defaults.timeout = 8081;
axios.defaults.baseURL = 'http://localhost:8081';
axios.crossDomain = true;

// axios.interceptors.request.use(config => {
//   config;
// }, error => {
//   Promise.reject(error);
// });
//
// axios.interceptors.response.use(response => {
//   response;
// }, error => {
//   console.log(error);
//   return Promise.reject(error);
// });

export default axios;
