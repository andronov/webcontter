import {action, observable} from "mobx";

export default class TemplateModel {
  id = Math.random();
  @observable title;
  @observable backgroundImage = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/25776/thumb-1920-595688.jpg';


  constructor() {
  }

}
