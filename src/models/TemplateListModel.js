import { observable, computed, action } from "mobx";
import TemplateModel from "./TemplateModel";

export default class TemplateListModel {
  @observable items = [];
  offset = 0;
  limit = 10;

  constructor(){
    this.items.push([new TemplateModel(), new TemplateModel()]);
  }
  /*@computed
  get unfinishedTodoCount() {
    return this.todos.filter(todo => !todo.finished).length;
  }

  @action
  addTodo(title) {
    this.todos.push(new TodoModel(title));
  }*/
}

