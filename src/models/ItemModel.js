import {action, observable} from "mobx";

export default class ItemModel {
  id = Math.random();
  @observable is_promo = true;
  @observable title;
  @observable finished = false;
  @observable backgroundImage = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/25776/thumb-1920-595688.jpg';


  constructor(title, backgroundImage, description) {
    this.title = title;
    this.backgroundImage = backgroundImage;

  }

}
