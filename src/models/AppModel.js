import {action, observable} from "mobx";
import request from '../api/request';
import ItemModel from "./ItemModel";
import TemplateListModel from "./TemplateListModel";

let menus = [
    {title: "Templates", active: false},
    {title: "Fonts", active: true},
    {title: "Effects", active: false},
    {title: "GIF", active: false},
  ];

export default class AppModel {
  id = Math.random();
  @observable item = null;
  @observable templates = null;
  @observable menus = menus;


  constructor() {
    //this.item = new ItemModel();
    //console.log('init', this)
    this.templates = new TemplateListModel()
  }

  @action
  setActiveMenu = (index) => {
    this.menus.map((item, i)=> {item.active = index === i; return item})
  };

  @action
  handleLink = (link) => {
    console.log('handleLink', link, this);
    request.post(`/link`, {link}).then(response => {
      console.log(response, this);
      this.item = new ItemModel(response.data.title, response.data.path, response.data.description);
      //this.title = response.data.title;
      //this.backgroundImage = response.data.path;
    }).catch(error => {
      console.log(error);
    });
  }
}
