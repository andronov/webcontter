import React from "react";
import { render } from "react-dom";
import { Provider } from "mobx-react";
import { AppContainer } from "react-hot-loader";
import { BrowserRouter as Router } from "react-router-dom";

import App from "./components/App";
import AppModel from "./models/AppModel";

const store = {
  app: new AppModel()
};

render(
  <AppContainer>
    <Router>
      <Provider store={store}>
        <App />
      </Provider>
    </Router>
  </AppContainer>,
  document.getElementById("root")
);

// playing around in the console
window.store = store;
