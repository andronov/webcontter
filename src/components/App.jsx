import React from "react";
import { observer } from "mobx-react";
import styles from './app.css';
import Editor from "./Editor/Editor";

@observer
class App extends React.Component {

  render() {
    return (
      <div className={styles.app}>
        <Editor />
      </div>
    );
  }
}

export default App;
