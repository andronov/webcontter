import {inject, observer} from "mobx-react";
import React from "react";
import styles from "./Item.css";

@inject("store")
@observer
class Item extends React.Component {

  render() {
    const { store: { app } } = this.props;
    const { item } = app;
    return (
      <div className={styles.item}>
        <div className={styles.item_img} style={{backgroundImage: `url(${item.backgroundImage})`}}/>
        <div className={styles.titles}>

          {<div className={styles.title}>
            <div className={styles["title-word"]} style={{fontSize: '16px'}}>{item.title}</div>
            {/*<div className={styles["title-word"]}>BEST DAY HIKE IN</div>*/}
            {/*<div className={styles["title-word"]}>THE DOLOMITES</div>*/}
          </div>}
        </div>
      </div>
    );
  }
}

export default Item;
