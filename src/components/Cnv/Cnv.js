import React from "react";
import {inject, observer} from "mobx-react";
import styles from './Cnv.css';
import { fabric } from 'fabric';

@inject("store")
@observer
class Cnv extends React.Component {

  componentDidMount = () => {
    this.fb();
  };

  fb = () => {
    let canvas = new fabric.Canvas('canvas', {containerClass: styles.canvas_container});




    let rect = new fabric.Rect({
      width: 300,
      height: 300,
      left: -200,
      top: 200,
      stroke: 'white',
      strokeWidth: 0,
      fill: 'rgba(254, 35, 128, 0.9)',
      selectable: false,
      angle: 45/*,
      shadow: {
        color: 'black',
        blur: 0,
        offsetX: 0,
        offsetY: 0
      }*/
    });
    canvas.add(rect);

    function oncom() {
      rect.animate('strokeWidth', rect.strokeWidth === 0 ? 3 : 0, {
        duration: 500,
        onChange: canvas.renderAll.bind(canvas),
        onComplete: function() {
        },
        easing: fabric.util.ease['easeOutQuad']
      });
    }

    rect.animate('left', rect.left === -200 ? 0 : -200, {
      duration: 2000,
      onChange: canvas.renderAll.bind(canvas),
      onComplete: function() {
        oncom();
        aftertext();
      },
      easing: fabric.util.ease['easeOutQuad']
    });

    let rect2 = new fabric.Rect({
      width: 100,
      height: 100,
      left: 370,
      top:50,
      stroke: 'rgba(2, 250, 201, 0.9)',
      strokeWidth: 0,
      fill: 'rgba(2, 250, 201, 0.9)',
      selectable: false,
      angle: 45
    });
    canvas.add(rect2);


    rect2.animate('left', 300, {
      duration: 2000,
      onChange: canvas.renderAll.bind(canvas),
      onComplete: function() {

      },
      easing: fabric.util.ease['easeOutQuad']
    });

    let textbox = new fabric.Textbox('BEST DAY THE DOLOMITES', {
      left: 5,
      top: 500,
      width: 170,
      fontSize: 26,
      fill: '#fff',
      //  textBackgroundColor: '#fff',
      fontFamily: "Helvetica",
      fontWeight: 'bold',
      selectable: true
    });
    canvas.add(textbox);

    function aftertext(){
      textbox.animate('top', 350, {
        duration: 500,
        onChange: canvas.renderAll.bind(canvas),
        onComplete: function() {
        },
        easing: fabric.util.ease['easeOutBounce']
      });
    }

  };

  render() {
    const { store: { app } } = this.props;
    return (
      <div className={styles.item}>
        <canvas ref={(e)=>{this.cnv = e}} id={"canvas"} width={300} height={500} className={styles.canvas}>

        </canvas>
        <div className={styles.canvas_image}/>
      </div>
    );
  }
}

export default Cnv;
