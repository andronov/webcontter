import React from "react";
import { observer } from "mobx-react";
import styles from './Editor.css';
import LeftEditor from "./LeftEditor/LeftEditor";
import RightEditor from "./RightEditor/RightEditor";

@observer
class Editor extends React.Component {

  render() {
    return (
      <div className={styles.editor}>
        <div className={styles.left}>
          <LeftEditor/>
        </div>
        <div className={styles.right}>
          <RightEditor/>
        </div>
      </div>
    );
  }
}

export default Editor;
