import React from "react";
import { observer, inject } from "mobx-react";
import styles from './Promo.css';

@inject("store")
@observer
class Promo extends React.Component {

  onClick = () => {
    const { store: { app } } = this.props;
    // item.handleLink('https://www.sports.ru/tribuna/blogs/fczenitspb/1777615.html')
  };

  onPaste = (e) => {
    //validate
    let value = e.clipboardData.getData('Text');
    const { store: { app } } = this.props;
    app.handleLink(value)
  };

  render() {
    return (
      <div className={styles.is_item}>
        <div className={styles.is_wrapper}>
          <div className={styles.demo}>
            <div className={styles.gradient}/>
          </div>
          <div className={styles.wave}>
            <svg viewBox="0 0 1440 120" aria-hidden="true">
              <path d="M1440,21.2101911 L1440,120 L0,120 L0,21.2101911 C120,35.0700637 240,42 360,42 C480,42 600,
                35.0700637 720,21.2101911 C808.32779,12.416393 874.573633,6.87702029 918.737528,4.59207306 C972.491685,
                1.8109458 1026.24584,0.420382166 1080,0.420382166 C1200,0.420382166 1320,7.35031847 1440,21.2101911 Z"/>
            </svg>
          </div>
        </div>

        <div className={styles.segment}>
          <div style={{color: 'white'}} className={styles.content}>
            Visual content
          </div>
          <div className={styles.form}>
            <input onPaste={this.onPaste} className={styles.input} placeholder={'Paste link'}/>
          </div>
        </div>
      </div>
    );
  }
}

export default Promo;
