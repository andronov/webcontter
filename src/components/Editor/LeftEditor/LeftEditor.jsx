import React from "react";
import { observer, inject } from "mobx-react";
import styles from './LeftEditor.css';
import Promo from "./Promo/Promo";
import Item from "../../Item/Item";
import Cnv from "../../Cnv/Cnv";

@inject("store")
@observer
class LeftEditor extends React.Component {

  render() {
    const { store: { app } } = this.props;
    const { item } = app;
    return (
      <div onClick={this.onClick} className={styles.column}>
        {/*<div className={styles.add}>add</div>*/}
        {/*!item && <Promo/>*/}
        {/*item && <Item/>*/}
        {<Cnv />}
      </div>
    );
  }
}

export default LeftEditor;
