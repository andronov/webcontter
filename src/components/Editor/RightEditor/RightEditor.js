import React from "react";
import {inject, observer} from "mobx-react";
import styles from './RightEditor.css';
import SearchEditor from "./SearchEditor/SearchEditor";
import TemplatesEditor from "./TemplatesEditor/TemplatesEditor";

@inject("store")
@observer
class RightEditor extends React.Component {

  render() {
    const { store: { app } } = this.props;
    const { menus } = app;
    return (
      <div className={styles.column}>
        <div className={styles.wrapper}>
          <div className={styles.header}>

            <div className={styles.menu}>
              <nav className={styles.nav}>
                {menus.map((item, i)=>{
                  return <div key={i}
                              onClick={() => app.setActiveMenu(i)}
                              style={{fontWeight: item.active ? 'bold' : 'normal'}}
                              className={styles.nav_item}>{item.title}
                              </div>
                })}
              </nav>
            </div>

            <SearchEditor />
          </div>

          <TemplatesEditor/>

        </div>
      </div>
    );
  }
}

export default RightEditor;
