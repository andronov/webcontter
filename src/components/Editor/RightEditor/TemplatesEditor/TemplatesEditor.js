import React from "react";
import { observer } from "mobx-react";
import styles from './TemplatesEditor.css';

@observer
class TemplatesEditor extends React.Component {

  render() {
    return (
      <div className={styles.items}>
        <div className={styles.item} style={{backgroundImage: 'url(https://si.wsj.net/public/resources/images/BN-LN503_snaptr_JV_20151202154847.jpg)'}}>

        </div>

        <div className={styles.item}>

        </div>

        <div className={styles.item}>

        </div>

        <div className={styles.item}>

        </div>

        <div className={styles.item}>

        </div>

        <div className={styles.item}>

        </div>
      </div>
    );
  }
}

export default TemplatesEditor;
