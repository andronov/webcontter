import React from "react";
import { observer, inject } from "mobx-react";
import styles from './SearchEditor.css';

@inject("store")
@observer
class SearchEditor extends React.Component {



  render() {
    const { store: { app } } = this.props;
    return (
      <div className={styles.search}>
        <div className={styles.wrapper}>
          New way for coming up with the best stories for you.
        </div>
      </div>
    );
  }
}

export default SearchEditor;
